Feature: Buyer Order
    buyer order produk by seller on mobile secondhand

    @positive_case
    Scenario: TC.Order.001-User can Order Product
        Given user already login
        And user is in "Beranda" page
        When user click one of cart product
        And user directed to "Detail Product" page
        And user click button "Saya tertarik dan ingin nego" 
        And user can see pop up "Input nego price"
        And user input nego price
        And user click button "Kirim"
        Then user success send negotiated price to seller
        And user get pop up notification success
        And button "Saya tertarik dan ingin nego" changed to button "Menunggu respon penjual"

    @positive_case
    Scenario: TC.Order.002-User can view detail product
        Given user already login
        And user is in "Beranda" page
        When user click of one cart product
        Then user directed to "Detail Product" page of the selected cart 

    @negative_case
    Scenario: TC.Order.003-Ensure users to register or login first before order product
        Given user not yet login
        And user is in "Detail Product" page
        When user click button "Saya tertarik dan ingin nego" 
        And user can see pop up "Input nego price"
        And user directed to page login
        And user fill field login with correct credential.
        And user success login
        And user directed to "Detail Product" page
        And user click button "Saya tertarik dan ingin nego" 
        And user can see pop up "Input nego price"
        And user input nego price
        And user click button "Kirim"
        Then user success send negotiated price to seller
        And user get pop up notification success
        And button "Saya tertarik dan ingin nego" changed to button "Menunggu respon penjual"

    @negative_case
    Scenario: TC.Order.004-User cannot buy product with nominal minus (-).
        Given user already login
        And user is in "Detail Product" page
        When user click button "Saya tertarik dan ingin nego"
        And user can see pop up "Input nego price"
        And user input nego price with nominal minus (-)
        Then user cannot input minus (-) amount.
        And field "Harga Tawar" integer

    @positive_case
    Scenario: TC.Order.005-User can search for the specific product category using the "search bar" feature
        Given user already login
        And user is in "Beranda" page
        When user click feature "Search Bar"
        And user inputted keyword search
        Then user can see product list based on the inputted keyword
