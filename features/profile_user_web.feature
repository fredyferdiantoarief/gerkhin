Feature: Profile User
  User upadte data profile

  Background:
     Given  User in homepage
     When User click profile
     Then User redirect profile page
     
    @positive_case
    Scenario:  User upadte data profile
       
        Then User upload image profile
        And User input name
        And User select kota
        And User input alamat
        And User input No Hanphone 081239123
        And User click button simpan
        Then User "success upadate data profile and redirect to homepage"

    @negative_case
    Scenario:  User update data profile without upload image profile
        And User input name
        And User select kota
        And User input alamat
        And User input No Hanphone 081239123
        And User click button simpan
        Then User "fail upadate data profile and reload page profile"

    @negative_case
    Scenario:  User update data profile without filling in the name field
        And User select kota
        And User input alamat
        And User input No Hanphone 081239123
        And User click button simpan
        Then User "fail update profle and user get alert please fill out this field"

    @negative_case
    Scenario:  User update data profile  without filling in the field kota
        And User input name
        And User input alamat
        And User input No Hanphone 081239123
        And User click button simpan
        Then User "fail update profle and user get alert please select an item in the list"

    @negative_case
    Scenario:  User update data profile then input Alphbet string in field No Hanphone
        And User input name
        And User input alamat
        And User input No Hanphone "ABDCASD"
        And User click button simpan
        Then User "success update profle and redirect to homepage"

    
    # Examples:
    # | status  |
    # | success upadate data profile and redirect to homepage |
    # | fail upadate data profile and reload page profile   |
    # | fail update profle and user get alert please fill out this field |
  
