Feature: Register

Background:
    Given User already opened the web page https://secondhand.binaracademy.org on the browser

Scenario: To ensure new user can create an account
    When User clicks "Masuk"
    And User clicks "Daftar di sini"
    And User fills Name field with "doni"
    And User fills Email field with "doni@email.com"
    And User fills Password field with "doni12345"
    And User clicks "Daftar"
    Then User successfully creates an account and is redirected to the homepage

Scenario: To ensure new user can't create an account with no name
    When User clicks "Masuk"
    And User clicks "Daftar di sini"
    And User fills Name field with " "
    And User fills Email field with "dona@email.com"
    And User fills Password field with "dona12345"
    And User clicks "Daftar"
    Then The pop-up appears saying "please fill out this field" on the Name field

Scenario: To ensure new user can't create an account with no email
    When User clicks "Masuk"
    And User clicks "Daftar di sini"
    And User fills Name field with "dona"
    And User fills Email field with " "
    And User fills Password field with "dona12345"
    And User clicks "Daftar"
    Then The pop-up appears saying "please fill out this field" on the Email field

Scenario: To ensure new user can't create an account with invalid email
    When User clicks "Masuk"
    And User clicks "Daftar di sini"
    And User fills Name field with "dona"
    And User fills Email field with "dona.email.com"
    And User fills Password field with "dona12345"
    And User clicks "Daftar"
    Then The pop-up appears saying "user must input the valid format email" on the Email Field

 Scenario: To ensure new user can't create an account with existing email
    When User clicks "Masuk"
    And User clicks "Daftar di sini"
    And User fills Name field with "dona"
    And User fills Email field with "doni@email.com"
    And User fills Password field with "dona12345"
    And User clicks "Daftar"
    Then The warning appears saying "Email has already been taken"