Feature: Profile User android
  User upadte data profile

    Background:
     Given  User in home page
     And User click menu Akun
     When User click icon pencil
     Then User redirect to Lengkapi Info Akun
     
    @positive_case
    Scenario:  To ensure that user can update data Name
         Then  User input Nama
         Then  User click button simpan
         And User success update data name and user get alert Profile berhasil diperbaharui

    @positive_case
    Scenario:  To ensure that user can update data nomor Hp
         Then  User input Nomor HP
         Then  User click button simpan
         And User success update data name and user get alert Profile berhasil diperbaharui

    @negative_case
    Scenario:  To ensure that user can update data Alamat with blank space
        Then  User input Alamat with blank space
        Then  User click button simpan
        And User fail update data Alamat and user get alert Wajib diisi

    @negative_case
    Scenario:  To ensure that user can update data Kota with blank space
        Then  User input Kota with blank space
        Then  User click button simpan
        And User fail update data Alamat and user get alert Wajib diisi

    @negative_case
    Scenario:  To ensure that user can't update data Nomor HP with value "N*"
        Then  User input Kota Nomor HP with value "N*"
        Then  User click button simpan
        And User success update data name and user get alert Profile berhasil diperbaharui