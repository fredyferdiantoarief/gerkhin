Feature: Buyer Order
    buyer order produk by seller on web secondhand

    Background: User already login
        Given user already login
        And user is in "Home" page

    @positive_case
    Scenario: TC.Order.001-User can Order Product
        When user click one of cart product
        And user directed to "Detail Product" page
        And user click button "Saya tertarik dan ingin nego" 
        And user input nego price
        And user click button "Kirim"
        Then user success send negotiated price to seller
        And button "Saya tertarik dan ingin nego" changed to button "Menunggu respon penjual"

    @positive_case
    Scenario: TC.Order.002-User can view detail product
        When user click of one cart product
        Then user directed to "Detail Product" page of the selected cart 

    @positive_case
    Scenario: TC.Order.003-User can explore cart product
        When user scroll "Home" page
        And user click button "Next" or "Previous"
        Then user can see cart product on the "Next" page or the "Previous" page 

    @positive_case
    Scenario: TC.Order.004-User can search for the specific product category using the "search bar" feature
        When user click feature "Search Bar"
        And user inputted keyword search
        Then user can see product list based on the inputted keyword

    @positive_case
    Scenario: TC.Order.005-User can search for product categories by click one of the category lists
        When user click on one category
        Then user can see cart product based on the chosen category
